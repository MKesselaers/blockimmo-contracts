pragma solidity 0.4.25;


contract Medianizer {
  /**
   * depending on your needs you can call `read()(bytes32)` but that call will revert if the price is invalid. You can also use `peek()(bytes32,bool)` and check the boolean value first, that will tell you if the price is correct. As for the encoding, the `bytes32` is just a `uint256` where the price is in wei, so ETH/USD of `609.59` would be `609590000000000000000
   */
  function read() public view returns (bytes32) {
    return 0x000000000000000000000000000000000000000000000020f39d027901310000;
  }
}
