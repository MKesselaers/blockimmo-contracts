import { assertRevert } from 'openzeppelin-solidity/test/helpers/assertRevert';

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');

contract('LandRegistry', ([from, other]) => {
  const eGrid = 'CH327417066524';
  const address = '0xC5A168eD2A712F7E5747f09A70524994D6D1687d'.toLowerCase();

  before(async function () {
    this.landRegistryProxy = await LandRegistryProxy.new({ from });
  });

  beforeEach(async function () {
    this.landRegistry = await LandRegistry.new({ from });
    const { logs } = await this.landRegistryProxy.set(this.landRegistry.address);
    logs[0].event.should.be.equal('Set');
    logs[0].args.landRegistry.should.be.equal(this.landRegistry.address);
    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);
  });

  describe('tokenizeProperty', () => {
    it('add a property to the land registry', async function () {
      const { logs } = await this.landRegistry.tokenizeProperty(eGrid, address, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('Tokenized');
      logs[0].args.eGrid.should.be.equal(eGrid);
      logs[0].args.property.should.be.equal(address);
    });

    it('only blockimmo can add to the land registry', async function () {
      await assertRevert(this.landRegistry.tokenizeProperty(eGrid, address, { from: other }));
    });

    it('E-Grid must be non-empty', async function () {
      await assertRevert(this.landRegistry.tokenizeProperty('', address, { from }));
    });

    it('property address must be non-zero', async function () {
      await assertRevert(this.landRegistry.tokenizeProperty(eGrid, 0x0, { from }));
    });

    it('E-Grid must be unique (property does not already exist in the land registry)', async function () {
      await this.landRegistry.tokenizeProperty(eGrid, address, { from }).should.be.fulfilled;
      await assertRevert(this.landRegistry.tokenizeProperty(eGrid, address, { from }));
    });

    it('adds multiple properties to the land registry', async function () {
      await this.landRegistry.tokenizeProperty(eGrid, address, { from }).should.be.fulfilled;
      await this.landRegistry.tokenizeProperty(eGrid.replace('C', 'D'), address.replace('c', 'd'), { from }).should.be.fulfilled;
    });
  });

  describe('getProperty', () => {
    it('get a property from the land registry', async function () {
      await this.landRegistry.tokenizeProperty(eGrid, address, { from }).should.be.fulfilled;
      (await this.landRegistry.getProperty.call(eGrid, { from: other })).should.be.equal(address);
    });

    it('E-Grid does not exist in the land registry', async function () {
      parseInt(await this.landRegistry.getProperty.call(eGrid, { from: other })).should.be.equal(0);
    });
  });

  describe('untokenizeProperty', () => {
    it('remove a property from the land registry', async function () {
      await this.landRegistry.tokenizeProperty(eGrid, address, { from }).should.be.fulfilled;

      const { logs } = await this.landRegistry.untokenizeProperty(eGrid, { from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('Untokenized');
      logs[0].args.eGrid.should.be.equal(eGrid);
      logs[0].args.property.should.be.equal(address);
    });

    it('only blockimmo can remove from the land registry', async function () {
      await this.landRegistry.tokenizeProperty(eGrid, address, { from }).should.be.fulfilled;
      await assertRevert(this.landRegistry.untokenizeProperty(eGrid, { from: other }));
    });

    it('E-Grid does not exist in the land registry', async function () {
      await assertRevert(this.landRegistry.untokenizeProperty(eGrid, { from }));
    });
  });
});
