import { assertRevert } from 'openzeppelin-solidity/test/helpers/assertRevert';

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const role = 'exampleRole';

contract('Whitelist', ([from, other]) => {
  before(async function () {
    this.whitelistProxy = await WhitelistProxy.new({ from });
  });

  beforeEach(async function () {
    this.whitelist = await Whitelist.new({ from });
    const { logs } = await this.whitelistProxy.set(this.whitelist.address);
    logs[0].event.should.be.equal('Set');
    logs[0].args.whitelist.should.be.equal(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);
  });

  describe('changing whitelist', () => {
    it('owner can add whitelist', async function () {
      await this.whitelist.checkRole(other, role).should.be.rejected;
      await this.whitelist.grantPermission(other, role).should.be.fulfilled;
      await this.whitelist.checkRole(other, role).should.be.fulfilled;
    });

    it('owner can revoke permission', async function () {
      await this.whitelist.grantPermission(other, role).should.be.fulfilled;
      await this.whitelist.revokePermission(other, role).should.be.fulfilled;
      await this.whitelist.checkRole(other, role).should.be.rejected;
    });

    it('owner can add permissions in batch', async function () {
      await this.whitelist.checkRole(from, role).should.be.rejected;
      await this.whitelist.checkRole(other, role).should.be.rejected;
      await this.whitelist.grantPermissionBatch([from, other], role).should.be.fulfilled;
      await this.whitelist.checkRole(from, role).should.be.fulfilled;
      await this.whitelist.checkRole(other, role).should.be.fulfilled;
    });

    it('owner can revoke permission in batch', async function () {
      await this.whitelist.grantPermissionBatch([from, other], role).should.be.fulfilled;
      await this.whitelist.revokePermissionBatch([from, other], role).should.be.fulfilled;
      await this.whitelist.checkRole(from, role).should.be.rejected;
      await this.whitelist.checkRole(other, role).should.be.rejected;
    });

    it('others can not add/remove permissions', async function () {
      await assertRevert(this.whitelist.grantPermission(other, role, { from: other }));
      await assertRevert(this.whitelist.grantPermissionBatch([from, other], role, { from: other }));

      await this.whitelist.grantPermissionBatch([from, other], role).should.be.fulfilled;
      await assertRevert(this.whitelist.revokePermission(from, role, { from: other }));
      await assertRevert(this.whitelist.revokePermissionBatch([from, other], role, { from: other }));
    });
  });
});
